"use strict";
let container = document.createElement('div')
const wrapper = document.querySelectorAll('.input-wrapper')
wrapper.forEach((element) => {
    element.addEventListener('click', (e) => {
        if (e.target.classList.contains('fa-eye')) {
            e.target.classList.remove('fa-eye')
            e.target.classList.add('fa-eye-slash')
        } else if (e.target.classList.contains('fa-eye-slash')) {
            e.target.classList.remove('fa-eye-slash')
            e.target.classList.add('fa-eye')
        }
        if (e.target.classList.contains('icon-password')) {

            if (e.target.previousElementSibling.getAttribute('type') === 'password') {
                e.target.previousElementSibling.setAttribute('type', 'text')
            } else {
                e.target.previousElementSibling.setAttribute('type', 'password')
            }
        }
    })
})
const btn = document.querySelector('.btn')
btn.addEventListener('click', (e) => {
        e.preventDefault()
        let input1 = document.querySelector('.input1')
        let input2 = document.querySelector('.input2')
        if (input1.value === input2.value && input1.value !== "" && input2.value !== "") {
            container.remove()
            alert('You are welcome')
        } else {
            btn.before(container)
            container.innerText = 'Потрібно ввести однакові значення'
            container.style.color = 'red'
        }
    }
)
