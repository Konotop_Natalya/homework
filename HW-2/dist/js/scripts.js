"use strict";
(function () {
    console.log(" ");
    document.querySelector(".burger-menu_close .fa-solid").addEventListener("click", (e) => {
        e.target.classList.toggle("fa-bars");
        e.target.classList.toggle("fa-xmark");
        const navbar = document.querySelector(".navbar")
        if (e.target.classList.contains("fa-xmark")) {
            navbar.style.display = "block";
        } else {
            navbar.style.display = "none"
        }
        ;
    })
}());
