'use strict'
import gulp from 'gulp';
import clean from 'gulp-clean';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import autoprefixer from 'gulp-autoprefixer'
import minifyJs from 'gulp-minify'
import cleanCSS from 'gulp-clean-css';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';
const sync = browserSync.create();
import purgecss from 'gulp-purgecss';

function cleanDist() {
    return gulp.src('./dist/*')
        .pipe(clean());
}

function cleanJs() {
    return gulp.src('./dist/**/*.js')
        .pipe(clean());
}
function cleanCss() {
    return gulp.src('./dist/**/*.css')
        .pipe(clean());
}

function scssToCss() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.min.css'))
        .pipe(purgecss({
            content: ['index.html'],
            safelist: ['menu_open', 'header__menu-icon2']
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'))
        .pipe(sync.stream());
}

function unionJs() {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(minifyJs({ext:{
                src:'.js',
                min:'.min.js'
            },compress: true}))
        .pipe(gulp.dest('./dist/js'))
        .pipe(sync.stream());
}

function unionImg() {
    return gulp.src('./src/img/**/*.{jpg,png,svg}')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img/'))
}
function serve() {
    sync.init({
        server: {
            baseDir: "./"
        },
    });
    gulp.watch('./src/**/*.scss', gulp.series(cleanCss,scssToCss)).on('change',sync.reload);
    gulp.watch('./src/**/*.js', gulp.series(cleanJs,unionJs)).on('change',sync.reload);
}

export const build = gulp.series(cleanDist,scssToCss,unionJs,unionImg);
export const dev = serve;