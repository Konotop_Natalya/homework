"use strict";
document.querySelector('ul').addEventListener('click', e => {
    if (e.target.tagName === "LI") {
        e.preventDefault();
        document.querySelectorAll("li.active, .tabsContents div.active")
            .forEach(element => {
                element.classList.remove('active');
            })
        e.target.classList.add('active');
        const id = e.target.getAttribute('data-id');
        document.querySelector(id).classList.add('active');
    }
})

$(document).ready(function () {
    $('.menu li a').click(function (event) {
        event.preventDefault();
        $('.menu li').removeClass('selected');
        $(this).parent('li').addClass('selected');
        let imgWidth = '278px';
        let imgHeight = '200px'
        let thisItem = $(this).attr('rel');

        if (thisItem !== "all") {
            $('.item li[rel=' + thisItem + ']').stop()
                .animate({
                    'width': imgWidth,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0
                });
            $('.item li[rel!=' + thisItem + ']').stop()
                .animate({
                    'width': 0,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0
                });
        } else {
            $('.item li').stop()
                .animate({
                    'opacity': 1,
                    'height': imgHeight,
                    'marginRight': 0,
                    'marginLeft': 0,
                    'width': imgWidth,
                });
        }
    })
    $('.item li img').animate({'opacity': 1}).hover(function () {
        $(this).animate({'opacity': 1});
    }, function () {
        $(this).animate({'opacity': 1});
    });
    $('.classForSlick').slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });
    $('.classForSlick2').slick({
        dots: false,
        centerMode: true,
        arrows: true,
        prevArrow: '.myNawLeft',
        nextArrow: '.myNawRight',
        infinite: true,
        speed: 300,
        slidesToScroll: 1,
        // slidesToShow: 4,
        adaptiveHeight: true,
        asNavFor: '.classForSlick',
        focusOnSelect: true,
        variableHeight: true,
        variableWidth: true
    });
});

let buttonLoadMore = document.querySelector("#loadMoreButton")
buttonLoadMore.addEventListener("click", () => {
    let hidenImg = document.querySelectorAll(".categoryItemsSecond")
    hidenImg.forEach((element) => {
        element.style.display = "block"

    })
    buttonLoadMore.style.display = "none";
})

let buttonRight = document.querySelector(".myNawRight")
let collecionMiniImg = document.querySelectorAll(".testimonialsUserImgSmallBlockCircle")
let collectionInfo = document.querySelectorAll(".testimonialsContent")
buttonRight.addEventListener("click", () => {
    for (let i = 0; i < collecionMiniImg.length; i++) {
        if (collecionMiniImg[i].classList.contains("activeMiniImg")) {
            collecionMiniImg[i].classList.remove("activeMiniImg")
            if (i < collecionMiniImg.length - 1) {
                collecionMiniImg[i + 1].classList.add("activeMiniImg")
                collectionInfo.forEach((element) => {
                    if (element.getAttribute("data-name") === collecionMiniImg[i + 1].getAttribute("data-name")) {
                        element.classList.add("activeFeedback")
                    } else {
                        element.classList.remove("activeFeedback")
                    }
                })
            } else {
                collecionMiniImg[0].classList.add("activeMiniImg")
                collectionInfo.forEach((element) => {
                    if (element.getAttribute("data-name") === collecionMiniImg[0].getAttribute("data-name")) {
                        element.classList.add("activeFeedback")
                    } else {
                        element.classList.remove("activeFeedback")
                    }
                })
            }
            return
        }
    }
})
let buttonLeft = document.querySelector(".myNawLeft")
buttonLeft.addEventListener("click", () => {
    for (let i = 0; i < collecionMiniImg.length; i++) {
        if (collecionMiniImg[i].classList.contains("activeMiniImg")) {
            collecionMiniImg[i].classList.remove("activeMiniImg")
            if (i === 0) {
                collecionMiniImg[collecionMiniImg.length - 1].classList.add("activeMiniImg")
                collectionInfo.forEach((element) => {
                    if (element.getAttribute("data-name") === collecionMiniImg[collecionMiniImg.length - 1].getAttribute("data-name")) {
                        element.classList.add("activeFeedback")
                    } else {
                        element.classList.remove("activeFeedback")
                    }
                })
            } else {
                collecionMiniImg[i - 1].classList.add("activeMiniImg")
                collectionInfo.forEach((element) => {
                    if (element.getAttribute("data-name") === collecionMiniImg[i - 1].getAttribute("data-name")) {
                        element.classList.add("activeFeedback")
                    } else {
                        element.classList.remove("activeFeedback")
                    }
                })
            }
            return
        }
    }
})
