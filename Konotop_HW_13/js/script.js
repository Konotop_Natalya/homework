// Технические требования:
// - В папке banners лежит HTML код и папка с картинками.
// - При запуске программы на экране отображается первая картинка.
// - Через 3 секунды вместо нее должна быть показана вторая картинка.
// - Еще через 3 секунды – третья.
// - Еще через 3 секунды – четвертая.
// - После того, как будут показаны все картинки – этот цикл должен начаться заново.
// - После запуска программы где-то на экране появится кнопка с надписью Прекратить.
// - После нажатия на кнопку Остановить цикл завершается, на экране остается показанной
// и картинка, которая была там при нажатии кнопки.
// - Рядом с кнопкой Остановить должна быть кнопка Восстановить показ, при нажатии
// которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// - разметку можно изменять, добавлять нужные классы, ID, атрибуты, теги.
"use strict";

let images = document.querySelectorAll(".image-to-show");
images[0].style.display = "block"
function timer (){
    for ( let i = 0; i < images.length; i++) {
        if (images[i].style.display === "block") {
            images[i].style.display = "none"
            if (i === images.length - 1) {
                images[0].style.display = "block"
            } else {
                images[i + 1].style.display = "block"}
            return;
        }
    }
}
let timerNew = setInterval(timer, 3000)
let buttonStart = document.querySelector( ".start")
let buttonStop = document.querySelector( ".stop")
buttonStart.setAttribute("disabled", true)
buttonStop.addEventListener("click", ()=>{
    clearInterval(timerNew)
    buttonStart.removeAttribute("disabled")
})
buttonStart.addEventListener("click", ()=> {
    timerNew = setInterval(timer, 3000)
    buttonStart.setAttribute("disabled", true)
})
