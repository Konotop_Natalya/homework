"use strict";
let userObject = {
    name: 'Nata',
    lastName: 'Konotop',
    birthday: {
        age: 30,
        zodiac: 'Aries',
    }
};
function cloneObject(userObject) {
    let newObject = {};
    for (let key in userObject) {
        if (typeof userObject[key] === "object") {
            newObject[key] = cloneObject(userObject[key]);
        } else {
            newObject[key] = userObject[key];
        }
    }
    return newObject;
}
let clonedObject = cloneObject(userObject);
console.log(clonedObject);
clonedObject.name = 'Sveta';
clonedObject.birthday.age = 25;