"use strict";
const list = document.querySelector(".list");
const displayingListCharacters = () => {
    fetch("https://ajax.test-danit.com/api/swapi/films")
        .then((response) => response.json())
        .then((arrFilms) => {
            arrFilms.forEach((film) => {
                console.log(getActorsPromises(film));
                Promise.all(getActorsPromises(film)).then((val) => {
                    renderShowingElements(film, val);
                });
            });
        });
};
const renderShowingElements = (film, val) => {
    let episodeId = createDOMElement(
        "ul",
        "Episod - " + film.episodeId
    );
    let name = createDOMElement(
        "p",
        "Name - " + film.name);
    let openingCrawl = createDOMElement(
        "p",
        "Content of the film - " + film.openingCrawl
    );
    let actors = createDOMElement(
        "p",
        "Actors - ");
    val.forEach((el) => {
        actors.textContent += el.name + " ";
    });
    list.append(episodeId);
    episodeId.append(name, actors, openingCrawl);
};
const getActorsPromises = (film) => {
    let response;
    return film.characters.map(
        (id) => (response = fetch(id).then((response) => response.json()))
    );
};
const createDOMElement = (tag, text) => {
    const elem = document.createElement(tag);
    elem.textContent = text;
    return elem;
};
displayingListCharacters();