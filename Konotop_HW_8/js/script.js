// Завдання
// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//
// 2.  Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//
// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
//
// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
//
//  5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
"use strict";
let nodeListP = document.querySelectorAll('p');
nodeListP.forEach(p => {
    p.style.backgroundColor = "#ff0000";
})

let ul = document.getElementById('optionsList');
console.log(ul);

let parens = ul.parentElement;
console.log(parens);

let children = ul.childNodes;
console.log(children);

let element = document.getElementById('testParagraph');
element.innerText = 'This is a paragraph';


let mainHeader = document.querySelector('.main-header');
let childrenHeader = mainHeader.children
for (let element of childrenHeader) {
    console.log(element);
    element.classList.add ('nav-item');
}

document.querySelector('.section-title').classList.remove('section-title');


