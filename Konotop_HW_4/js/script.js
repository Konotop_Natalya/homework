"use strict";
let number1 = + prompt("Input number1");
while (!number1) {
    number1 = + prompt("Input number1", number1);
}
let number2 = + prompt("Input number2");
while (!number2) {
    number2 = + prompt("Input number2", number2);
}
let operation = prompt('Input mathematical operation');
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt('You can also enter +, -, *, /.')
}
function mathematicalOperation (number1, number2, operation) {
    if (operation === '+') {
        return number1 + number2;
    }
    if (operation === '-') {
        return number1 - number2;
    }
    if (operation === '*') {
        return number1 * number2;
    }
    if (operation === '/') {
        return number1 / number2;
    }
}
console.log(mathematicalOperation(number1, number2, operation));