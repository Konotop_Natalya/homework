"use strict";

async function getUsers() {
    const response = await fetch("https://ajax.test-danit.com/api/json/users");
    const usersUrl = await response.json();
    return usersUrl;
}


async function getPosts() {
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts`);
    const postsUrl = await response.json();
    return postsUrl;
}

class Card {
    constructor(name, email, title, text, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.id = id;
    }
    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) element.textContent = text;
        element.classList.add(...classNames);
        return element;
    }
    renderHeader(container) {
        const header = this.createElement("div", ["card-header"]);
        header.insertAdjacentHTML(
            "afterbegin",
            `<a href=# class= 'card-name'> ${this.name} </a>
        <div class="btn-del"> 
        <img src="cross.png" alt="logo">
        </div>  
        <div class="card-email"> ${this.email} </div>`
        );
        container.append(header);
        header.addEventListener("click", (e) => {
            const deleteTarget = e.target.closest(".btn-del");
            if (deleteTarget) {
                const toDelUser = confirm("Delete post?");
                if (toDelUser) {
                    this.card.remove();
                    fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                        method: "DELETE",
                    });
                }
            }
        });
    }
    renderText(container) {
        const text = this.createElement("div", ["card-body"]);
        text.insertAdjacentHTML(
            "afterbegin",
            `<div class="card-title"> ${this.title} </div> 
                  <div class="card-text"> ${this.text} </div>`
        );
        container.append(text);
    }
    render() {
        this.card = this.createElement("div", ["card-container"]);
        document.querySelector(".container").append(this.card);
        this.renderHeader(this.card);
        this.renderText(this.card);
        return this.card;
    }
}
function displayPosts() {
    getUsers().then((users) => {
        getPosts().then((posts) => {
            users.forEach((user) => {
                posts.forEach((post) => {
                    if (user.id === post.userId) {
                        let card = new Card(
                            user.name,
                            user.email,
                            post.title,
                            post.body,
                            post.id
                        );
                        card.render();
                    }
                });
            });
        });
    });
}

displayPosts();

