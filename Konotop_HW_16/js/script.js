"use strict";
function fibonacci(number, firstNumber, secondNumber) {
    if (number === 2) {
        return secondNumber;
    } else if (number === 1) {
        return firstNumber;
    } else {
        let f1 = firstNumber, f2 = secondNumber, f3;
        for (let i = 2; i < number; i++) {
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
        return f3;
    }
}
function fibonacciMinus(number, firstNumber, secondNumber) {
    if (number === 0) {
        return;
    } else {
        let f1 = firstNumber, f2 = secondNumber, f3;
        for (let i = 2; i < number; i++) {
            f3 = f1 - f2;
            f1 = f2;
            f2 = f3;
        }
        return f3;
    }
}
let firstNumber = +prompt('Enter first number');
while ((firstNumber.toFixed() != firstNumber) || isNaN(firstNumber) || firstNumber == "") {
    firstNumber = +prompt('Enter first number again', '');
}
let secondNumber = +prompt('Enter second number');

while ((secondNumber.toFixed() != secondNumber) || isNaN(secondNumber || secondNumber == "")) {
    secondNumber = +prompt('Enter second number again', '');
}
let number = +prompt('Enter number:');
while ((number.toFixed() != number) || isNaN(number) || number == "") {
    number = +prompt('Enter number again', '');
}
if (firstNumber > 0 && secondNumber > 0) {
    alert(`Fibonacci number - ${fibonacci(number, firstNumber, secondNumber)}`);
} else {
    alert(`Fibonacci number - ${fibonacciMinus(number, firstNumber, secondNumber)}`);
}
